#include "WiFi.h"
#include "AsyncUDP.h"
#include "esp_wifi.h"
#include "esp_wifi_types.h"
#include "ArduinoJson.h"
#include "ETH.h"

#include <esp_now.h>

/*
#include "U8g2lib.h"

#ifdef U8X8_HAVE_HW_SPI
#include "SPI.h"
#endif
#ifdef U8X8_HAVE_HW_I2C
#include "Wire.h"
#endif
*/

/*
#ifdef ETH_CLK_MODE
#undef ETH_CLK_MODE
#endif
#define ETH_CLK_MODE    ETH_CLOCK_GPIO17_OUT

// Pin# of the enable signal for the external crystal oscillator (-1 to disable for internal APLL source)
#define ETH_POWER_PIN   17

// Type of the Ethernet PHY (LAN8720 or TLK110)
#define ETH_TYPE        ETH_PHY_LAN8720

// I²C-address of Ethernet PHY (0 or 1 for LAN8720, 31 for TLK110)
#define ETH_ADDR        1

// Pin# of the I²C clock signal for the Ethernet PHY
#define ETH_MDC_PIN     23

// Pin# of the I²C IO signal for the Ethernet PHY
#define ETH_MDIO_PIN    18
*/

const char * ssid = "kkkkk";
const char * password = "12345678";

AsyncUDP udp;
AsyncUDP udp1;


DynamicJsonDocument json(512);
//U8G2_UC1609_SLG19264_F_4W_HW_SPI u8g2(U8G2_R0, /* cs=*/ 5, /* dc=*/ 17, /* reset=*/ 16);
//U8G2_UC1609_SLG19264_F_4W_SW_SPI u8g2(U8G2_R0, /* clock=*/ 14, /* data=*/ 13, /* cs=*/ 5, /* dc=*/ 16, /* reset=*/ 32);


hw_timer_t *timer = NULL;

static bool eth_connected = false;
int bytes;
String msg;
uint8_t* last_packet_data;
size_t  last_packet_size;
bool sent = true;
int bytes_second;
volatile SemaphoreHandle_t timerSemaphore;

int rssi_total = 0;
int rssi_num = 0;

typedef struct
{
    unsigned frame_ctrl : 16;  // 2 bytes / 16 bit fields
    unsigned duration_id : 16; // 2 bytes / 16 bit fields
    uint8_t addr1[6];          // receiver address
    uint8_t addr2[6];          //sender address
    uint8_t addr3[6];          // filtering address
    unsigned sequence_ctrl : 16; // 2 bytes / 16 bit fields
} wifi_ieee80211_mac_hdr_t;    // 24 bytes

typedef struct
{
    wifi_ieee80211_mac_hdr_t hdr;
    unsigned category_code : 8; // 1 byte / 8 bit fields
    uint8_t oui[3]; // 3 bytes / 24 bit fields
    uint8_t payload[0];
} wifi_ieee80211_packet_t;

IRAM_ATTR void onPacket(AsyncUDPPacket& packet) {
//    String data(packet.data(), packet.length());
    udp.writeTo(packet.data(), packet.length(), IPAddress(192,168,1,25), 8888, TCPIP_ADAPTER_IF_ETH);
    deserializeJson(json, packet.data(), packet.length());
    printf("%d RSSI: %d Size: %zu\n", json["timestamp"].as<int>(), WiFi.RSSI(), packet.length());
    bytes += packet.length();
//    printf("%d\n", udp1.getWriteError());

    /*
    last_packet_data = packet.data();
    last_packet_size = packet.length();
    sent = false;
    */
}
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int length) {
//    udp.writeTo(incomingData, length, IPAddress(192,168,1,130), 8888, TCPIP_ADAPTER_IF_ETH);
    String data(incomingData, length);
//    if(data != "") {
//        printf("%s %dB/s\n", data.c_str(), bytes_second);
//    }
    if(eth_connected) {
        udp.writeTo(incomingData, length, IPAddress(192, 168, 1, 25), 8888, TCPIP_ADAPTER_IF_ETH);
    }
    bytes += length;
}

void IRAM_ATTR onTimer(){
    msg = "{\"rate\":";
    msg += bytes;
    msg += ", \"rssi\":";
    if (rssi_num == 0){
        msg += 0;
    }
    else{
        msg += String((float)rssi_total / (float)rssi_num);
    }
    msg += "}";
    bytes_second = bytes*10;
    bytes = 0;
    rssi_num = 0;
    rssi_total = 0;
    xSemaphoreGiveFromISR(timerSemaphore, NULL);
}

void ethernetEvent(WiFiEvent_t event) {
    switch (event) {
        case ARDUINO_EVENT_ETH_START:
            Serial.println("ETH Started");
            //set eth hostname here
            ETH.setHostname("esp32-ethernet");
            break;
        case ARDUINO_EVENT_ETH_CONNECTED:
            Serial.println("ETH Connected");
            break;
        case ARDUINO_EVENT_ETH_GOT_IP:
            Serial.print("ETH MAC: ");
            Serial.print(ETH.macAddress());
            Serial.print(", IPv4: ");
            Serial.print(ETH.localIP());
            if (ETH.fullDuplex()) {
                Serial.print(", FULL_DUPLEX");
            }
            Serial.print(", ");
            Serial.print(ETH.linkSpeed());
            Serial.println("Mbps");
            eth_connected = true;
            break;
        case ARDUINO_EVENT_ETH_DISCONNECTED:
            Serial.println("ETH Disconnected");
            eth_connected = false;
            break;
        case ARDUINO_EVENT_ETH_STOP:
            Serial.println("ETH Stopped");
            eth_connected = false;
            break;
        default:
            break;
    }
}
void promiscuous_rx_cb(void *buf, wifi_promiscuous_pkt_type_t type){
    if (type != WIFI_PKT_MGMT){
        return;
    }
    static const uint8_t ACTION_SUBTYPE = 0xd0;
    static const uint8_t ESPRESSIF_OUI[] = {0x18, 0xfe, 0x34};

    const wifi_promiscuous_pkt_t *ppkt = (wifi_promiscuous_pkt_t *)buf;
    const wifi_ieee80211_packet_t *ipkt = (wifi_ieee80211_packet_t *)ppkt->payload;
    const wifi_ieee80211_mac_hdr_t *hdr = &ipkt->hdr;

    if ((ACTION_SUBTYPE == (hdr->frame_ctrl & 0xFF)) &&
        (memcmp(ipkt->oui, ESPRESSIF_OUI, 3) == 0)) {

        int rssi = ppkt->rx_ctrl.rssi;
//        printf("rssi: %d\n", rssi);
        rssi_total += rssi;
        rssi_num++;
    }


}

void setup()
{
//    digitalWrite(ETH_POWER_PIN, LOW);
    Serial.begin(115200);
    delay(500);
//    u8g2.begin();
//    u8g2.clearDisplay();

    timerSemaphore = xSemaphoreCreateBinary();
    timer = timerBegin(0, 80, true);
    timerAttachInterrupt(timer, &onTimer, true);
    timerAlarmWrite(timer, 100000, true);
    timerAlarmEnable(timer);

    WiFi.onEvent(ethernetEvent);
    ETH.begin();
    ETH.config(IPAddress(192, 168, 1, 102),IPAddress(192, 168, 1, 1),IPAddress(255, 255, 255, 0),IPAddress(192, 168, 1, 1));


    wifi_init_config_t my_config = WIFI_INIT_CONFIG_DEFAULT(); //We use the default config ...
    my_config.static_rx_buf_num = 32;
    esp_wifi_init(&my_config);                                 //set the new config
    esp_wifi_start();

    esp_wifi_set_promiscuous(true);
    esp_wifi_set_promiscuous_rx_cb(&promiscuous_rx_cb);
    WiFi.setSleep(false);
    WiFi.setTxPower(WIFI_POWER_19_5dBm);
    WiFi.mode(WIFI_STA);

    // Init ESP-NOW
    if (esp_now_init() != ESP_OK) {
        Serial.println("Error initializing ESP-NOW");
        return;
    }
    esp_now_register_recv_cb(OnDataRecv);


}

void loop()
{
    /*
    u8g2.clearBuffer();
    u8g2.setFont(u8g2_font_helvR08_te);
//    if ( WiFi.status() != WL_CONNECTED )
//    {
//        u8g2.drawStr(0,10, "Disconnected");
//        u8g2.sendBuffer();
//        WiFi.begin( ssid, password );
//        while( WiFi.status() != WL_CONNECTED ) {
//            delay( 500 );
//        }
//    }
    					// clear the internal memory
    	// choose a suitable font
    u8g2.drawStr(0,10, "Connected");
    String rssi = "RSSI: ";
    rssi += WiFi.RSSI();
    u8g2.drawStr(0,20, rssi.c_str());
//    u8g2.drawStr(0,30, WiFi.localIP().toString().c_str());	// write something to the internal memory
    u8g2.drawStr(0,30,msg.c_str());
    u8g2.updateDisplay();
     */

    if (eth_connected && xSemaphoreTake(timerSemaphore, 0) == pdTRUE) {
        udp.writeTo((uint8_t *) msg.c_str(), msg.length(), IPAddress(192, 168, 1, 25), 7777, TCPIP_ADAPTER_IF_ETH);
        printf("%s\n",  msg.c_str());
    }

//    if (xSemaphoreTake(timerSemaphore, 0) == pdTRUE) {
//        printf("%dB/s\n",  bytes_second);
//    }
}